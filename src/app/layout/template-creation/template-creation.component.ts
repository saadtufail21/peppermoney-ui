import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { TemplateService } from '../../services/template.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomValidator } from '../../../assets/shared/validation';

@Component({
  selector: 'app-template-creation',
  templateUrl: './template-creation.component.html',
  styleUrls: ['./template-creation.component.scss']
})
export class TemplateCreationComponent implements OnInit {

  generalInfoForm: FormGroup;
  tablesGroup: FormGroup;
  tempTables: FormArray;
  numberOfTables: number;
  dataTypes = ['Text', 'DropDown List', 'CheckBox'];
  ddlObject = [];

  constructor(
    private templateService: TemplateService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.generalInfoForm = new FormGroup({
      templateName: new FormControl('', [Validators.required]),
      sourceNumber: new FormControl('', [Validators.required, Validators.min(1)]),
      destinationNumber: new FormControl('', [Validators.required, Validators.min(1)]),
      additionalNumber: new FormControl('', [Validators.required, Validators.min(1)]),
      toggle: new FormControl({ disable: false }),
      additionalTables: this.formBuilder.array([])
    });
    this.tablesGroup = new FormGroup({
      tempTables: this.formBuilder.array([])
    });
  }

  getArray(num) {
    return num > 0 ? new Array(num).fill(0) : null;
  }

  createAdditionalInfoItems(event) {
    const additionalTablesInfo = this.generalInfoForm.get('additionalTables') as FormArray;
    additionalTablesInfo.length > event.target.value ?
      this.getArray(additionalTablesInfo.length - event.target.valueAsNumber).forEach((val, index) => {
        additionalTablesInfo.removeAt(additionalTablesInfo.length - 1 - index);
      }) :
      this.getArray(event.target.valueAsNumber - additionalTablesInfo.length).forEach(() => {
        additionalTablesInfo.push(this.formBuilder.group({
          name: ['', [Validators.required]],
          number: ['', [Validators.required, Validators.min(1)]]
        }));
      });
  }

  createAdditionalTables() {
    this.numberOfTables = 2 + this.generalInfoForm.get('additionalNumber').value;
    const tempTables = this.tablesGroup.get('tempTables') as FormArray;
    tempTables.clear();
    this.getArray(this.numberOfTables).forEach(() => {
      tempTables.push(new FormGroup({
        tableName: new FormControl('', [Validators.required]),
        propertiesNumber: new FormControl('', Validators.required),
        fieldName: new FormControl([], [Validators.required]),
        dataType: new FormControl([], Validators.required),
        defaultValue: new FormControl([])
      }));
    });
    this.initializeAdditionalTables();
  }

  initializeAdditionalTables() {
    const tempTables = this.tablesGroup.get('tempTables') as FormArray;
    const sourceNumber = this.generalInfoForm.get('sourceNumber');
    const destinationNumber = this.generalInfoForm.get('destinationNumber');
    const additionalNumber = this.generalInfoForm.get('additionalNumber');
    const additionalTablesInfo = this.generalInfoForm.get('additionalTables') as FormArray;

    // source
    tempTables.controls[0].setValue({
      tableName: 'Source Properties',
      propertiesNumber: sourceNumber.value,
      fieldName: new Array(sourceNumber.value),
      dataType: new Array(sourceNumber.value).fill('Text'),
      defaultValue: new Array(sourceNumber.value).fill('')
    });

    // destination
    tempTables.controls[1].setValue({
      tableName: 'Destination Properties',
      propertiesNumber: destinationNumber.value,
      fieldName: new Array(destinationNumber.value),
      dataType: new Array(destinationNumber.value).fill('Text'),
      defaultValue: new Array(destinationNumber.value).fill('')
    });

    // additional
    additionalNumber.value > 0 ?
      additionalTablesInfo.value.forEach((table, index) => {
        tempTables.controls[2 + index].setValue({
          tableName: table.name,
          propertiesNumber: table.number,
          fieldName: new Array(table.number),
          dataType: new Array(table.number).fill('Text'),
          defaultValue: new Array(table.number).fill('')
        });
      }) : '';
  }

  onAdditionalToggleChange() {
    if (this.generalInfoForm.controls['toggle'].value) {
      this.generalInfoForm.controls['additionalNumber'].enable();
    } else {
      this.generalInfoForm.controls['additionalNumber'].disable();
      this.generalInfoForm.controls['additionalNumber'].setValue(0);
      const additional = this.generalInfoForm.get('additionalTables') as FormArray;
      additional.clear();
    }
  }

  fieldNameUpdate(tableIndex, fieldIndex, event) {
    this.tablesGroup.get('tempTables').value[tableIndex].fieldName[fieldIndex] = event.target.value;
  }

  sourceTypeUpdate(tableIndex, fieldIndex, event) {
    this.tablesGroup.get('tempTables').value[tableIndex].dataType[fieldIndex] = event.value;
  }

  // defaultValueUpdate(tableIndex, fieldIndex, event) {
  //   if (this.tablesGroup.get('tempTables').value[tableIndex].dataType[fieldIndex] === 'DropDown List') {
  //     const valueArray = event.target.value.split(',');
  //     this.tablesGroup.get('tempTables').value[tableIndex].defaultValue[fieldIndex] = valueArray;
  //   } else {
  //     this.tablesGroup.get('tempTables').value[tableIndex].defaultValue[fieldIndex] = event.target.value;
  //   }
  // }

  // defaultValueUpdate(tableIndex, fieldIndex, event) {
  //   this.tablesGroup.get('tempTables').value[tableIndex].defaultValue[fieldIndex] = event.target.value;
  //   console.log(event.target.value);
  // }
  defaultValueUpdate(tableIndex, fieldIndex, event) {
    if (this.tablesGroup.get('tempTables').value[tableIndex].dataType[fieldIndex] === 'DropDown List') {
      const valueArray = event.target.value.split(',');
      this.ddlObject.push({
        'attributeName': this.tablesGroup.get('tempTables').value[tableIndex].fieldName[fieldIndex],
        'attributeValues': valueArray
      });
    }
    this.tablesGroup.get('tempTables').value[tableIndex].defaultValue[fieldIndex] = event.target.value;
    // console.log('DDL Object: ', this.ddlObject);
    // console.log('DDL Object: ', JSON.stringify(this.ddlObject));

  }

  finish() {
    const obj = {
      templateName: this.generalInfoForm.get('templateName').value,
      tempTables: this.tablesGroup.value['tempTables']
    };

    // var blob = new Blob([JSON.stringify(obj)], {type: 'text/json'}),
    // e    = document.createEvent('MouseEvents'),
    // a    = document.createElement('a')

    // a.download = "setup.json";
    // a.href = window.URL.createObjectURL(blob);
    // a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
    // e.initEvent('click', true, false);
    // a.dispatchEvent(e);

    // console.log(JSON.stringify(obj));



    // obj = {
    //   templateName: 'test',
    //   tempTables : [
    //     {
    //     "tableName":"Source Properties",
    //     "propertiesNumber":2,
    //     "fieldName":["text","number"],
    //     "dataType":["text","number"],
    //     "defaultValue":["text","1"]
    //     },
    //     {
    //     "tableName":"Destination Properties",
    //     "propertiesNumber":3,
    //     "fieldName":["dn1","dn2","dn3"],
    //     "dataType":["DropDown List","CheckBox","CheckBox"],
    //     "defaultValue":[['a','b','c'],"true","false"]
    //     },
    //     {
    //     "tableName":"extra 1",
    //     "propertiesNumber":2,
    //     "fieldName":["date","time"],
    //     "dataType":["Date","Time"],
    //     "defaultValue":["ed1"]
    //     }
    //   ]
    // }

    // Save the DDL Object to Get all Attribute API.
    this.templateService.addDDLToAdmin(this.ddlObject)
      .subscribe(
        (data) => {
          // console.log('DDL Object Added');
          // console.log(data);
          // this._snackBar.open('Values Added Successfully.', 'Close', {
          //   duration: 3000,
          // });
        },
        (error) => {
          // console.log('Failed to add DDL Object');
          // this._snackBar.open('Failed to create Template.', 'Close', {
          //   duration: 3000,
          // });
        }
      );


    this.templateService.addTemplate(JSON.stringify(obj))
      .subscribe(
        (data) => {
          // console.log(data);
          this._snackBar.open('Template Created Successfully.', 'Close', {
            duration: 3000,
          });

        },
        (error) => {
          if (error.error.message === 'template already exist') {
            this._snackBar.open('Template already exist.', 'Close', {
              duration: 3000,
            });
          } else {
            this._snackBar.open('Failed to create Template.', 'Close', {
              duration: 3000,
            });
          }

        }
      );
  }
}
