import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms'
import { SessionService } from '../../services/session.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  title = 'app';
  user;
  public loginError: boolean;
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  loginForm: FormGroup = new FormGroup({
    username: this.username,
    password: this.password,
  });

  constructor(
    private sessionService: SessionService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }


  ngOnInit() { }

  getErrorMessage(field) {
    return this.loginForm.get(field).hasError('required') ? 'Must enter a value' : '';
  }

  submit() {
    this.sessionService.login(this.loginForm.value)
      .subscribe(
        (data) => {
          const response: any = data;
          // console.log(data);
          this.loginError = false;
          sessionStorage.setItem('token', response.token);

          this.router.navigateByUrl('/new-job');
          this._snackBar.open('Successful Login.', 'Close', {
            duration: 3000,
          });

          // this.user = data
        },
        (error) => {
          this.loginError = true;
          this._snackBar.open('Failed to Login.', 'Close', {
            duration: 3000,
          });

        }
      )
  }

}
