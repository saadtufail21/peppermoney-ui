import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { CronOptions } from 'cron-editor/lib/CronOptions';
import { HistoryService } from '../../services/history.service';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.scss']
})
export class EditJobComponent implements OnInit {

  job;
  dataForm: FormGroup;
  scheduleForm: FormGroup;
  startDate;
  cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',
    defaultTime: '10:00:00',
    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,
    use24HourTime: true,
    hideSeconds: false,
    removeSeconds: false,
    removeYears: false,
  };
  finalObject;
  scheduled = false;
  schemaAvailable;
  schemaString;
  generalProperties = ['Country', 'Source System', 'Source Table', 'Template', 'Lineage Mode'];

  getEditProcessId = '';
  // If everything works fine replace getEditProcessId with job

  constructor(
    private formBuilder: FormBuilder,
    private historyServcice: HistoryService,
    private _snackBar: MatSnackBar

  ) {

  }

  ngOnInit() {
    this.dataForm = new FormGroup({
      tempTables: this.formBuilder.array([]),
      scheduleForm: new FormGroup({
        cronExpression: new FormControl(''),
        startDate: new FormControl(''),
        endDate: new FormControl('')
      })
    });

    // this.scheduleForm = new FormGroup
    this.getEditProcessId = sessionStorage.getItem('editProcessGroupId');
    // console.log(sessionStorage.getItem('editProcessGroupId'));
    this.historyServcice.getEditJobDetails(this.getEditProcessId).subscribe((data) => {
      // console.log(data);
      this.job = data;
      this.formatJob();
      // console.log(this.dataForm);
    },
      (error) => {
        this._snackBar.open('Failed to Load Edit Data', 'Close', {
          duration: 3000,
        });
      });
    // this.getEditProcessId = sessionStorage.getItem('editProcessGroupId');
    // console.log(sessionStorage.getItem('editProcessGroupId'));
    // this.historyServcice.getEditJobDetails(this.getEditProcessId).subscribe((data) => {


    // },
    //   (error) => {

    //   });


    // Hardcoded values for Job.
    // this.job =
    // {
    //   'flowId' : 3737,
    //   'flowName' : 'jackson-test_ui_5676456_001',
    //   'version' : 1,
    //   'processGroupId' : '182f5e64-0171-1000-7adf-08a264db062f',
    //   'jobCreatedDate' : '2020-03-26',
    //   'jobUpdateDate' : '',
    //   'jobLastRunDate' : '',
    //   'oozieJobId' : null,
    //   'active' : false,
    //   'userId' : 'sa_aad_abaldua@pepintelfactory.com',
    //   'properties' : [
    //     {
    //       'id' : 3738,
    //       'key' : 'country',
    //       'value' : 'au',
    //       'category' : 'General Settings'
    //     }, {
    //       'id' : 3739,
    //       'key' : 'sourceSystem',
    //       'value' : 'jackson',
    //       'category' : 'General Settings'
    //     }, {
    //       'id' : 3740,
    //       'key' : 'sourceTable',
    //       'value' : 'test_ui_5676456_001',
    //       'category' : 'General Settings'
    //     }, {
    //       'id' : 3741,
    //       'key' : 'template',
    //       'value' : 'api-to-adls',
    //       'category' : 'General Settings'
    //     }, {
    //       'id' : 3742,
    //       'key' : 'lineageMode',
    //       'value' : 'File',
    //       'category' : 'General Settings'
    //     }, {
    //       'id' : 3468,
    //       'key' : 'scheduleType',
    //       'value' : 'SCHEDULED',
    //       'category' : 'schedule'
    //     }, {
    //       'id' : 3469,
    //       'key' : 'cron',
    //       'value' : '16 6 1/1 * *',
    //       'category' : 'schedule'
    //     }, {
    //       'id' : 3470,
    //       'key' : 'startDate',
    //       'value' : '2020-03-26T00:00:00.000Z',
    //       'category' : 'schedule'
    //     }, {
    //       'id' : 3471,
    //       'key' : 'endDate',
    //       'value' : '2020-03-27T00:00:00.000Z',
    //       'category' : 'schedule'
    //     }, {
    //       'id' : 3744,
    //       'key' : 'Source',
    //       'value' : 'api',
    //       'category' : 'Source Properties'
    //     }, {
    //       'id' : 3745,
    //       'key' : 'API URL',
    //       'value' : 'https://api.worldbank.org/v2/country/us?format=json',
    //       'category' : 'Source Properties'
    //     }, {
    //       'id' : 3746,
    //       'key' : 'Destination',
    //       'value' : 'adls',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3747,
    //       'key' : 'Client ID',
    //       'value' : 'ebf73456-443e-4986-941b-057906d25e2f',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3748,
    //       'key' : 'Client Key',
    //       'value' : 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3749,
    //       'key' : 'Auth Token Endpoint',
    //       'value' : 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3750,
    //       'key' : 'Account FQDN',
    //       'value' : 'pepperadlsuat.azuredatalakestore.net',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3751,
    //       'key' : 'Destination File Directory',
    //       'value' : '',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3752,
    //       'key' : 'Target Layer',
    //       'value' : 'raw',
    //       'category' : 'Destination Properties'
    //     }, {
    //       'id' : 3753,
    //       'key' : 'Target Path',
    //       'value' : '/raw/au/jackson',
    //       'category' : 'Destination Properties'
    //     },{
    //       id: 179,
    //       key: 'Schema Available',
    //       value: 'false',
    //       category: 'Destination Properties'
    //     },{
    //       id: 3988,
    //       key: 'schema',
    //       'value': 'one:two,three:four',
    //       'category': 'Schema'
    //     } ]
    // }

    // // Commenting this and calling this from inside api.
    // this.formatJob();
  }

  // API Data
  formatJob() {
    const tempTables = this.dataForm.get('tempTables') as FormArray;
    const scheduleForm = this.dataForm.get('scheduleForm') as FormGroup;
    let catArray: Array<any> = [];
    let propNumArray: Array<any> = [];

    this.job.properties.forEach((element, index) => {
      if (element.key === 'scheduleType' && element.value === 'SCHEDULED') {
        this.scheduled = true;
      }

      if (element.key === 'schema') {
        this.schemaAvailable = true;
      }
    });

    this.job.properties.forEach((element: {}) => {
      catArray.push(element['category']);
    });
    catArray = Array.from(new Set(catArray.map((item: any) => item)));
    propNumArray = this.getArray(catArray.length);

    catArray.forEach((item, index) => {
      this.job.properties.forEach((element: {}) => {
        element['category'] === item ? propNumArray[index]++ : '';
      });
    });

    catArray.forEach(() => {
      tempTables.push(new FormGroup({
        tableName: new FormControl(''),
        propertiesNumber: new FormControl(''),
        fieldID: new FormControl([]),
        fieldName: new FormControl([]),
        defaultValue: new FormControl([])
      }));
    });

    tempTables.controls.forEach((control, index) => {
      control.patchValue({
        tableName: catArray[index],
        propertiesNumber: propNumArray[index],
        // fieldName: new Array(propNumArray[index]),
        // dataType: new Array(propNumArray[index]),
        // defaultValue: new Array(propNumArray[index])
      });
    });

    tempTables.controls.forEach((table, index) => {
      this.job.properties.forEach((element: {}) => {
        if (table['controls']['tableName'].value === element['category']) {
          table['controls']['fieldID'].value.push(element['id']);
          table['controls']['fieldName'].value.push(element['key']);
          table['controls']['defaultValue'].value.push(element['value']);
        }
      });
    });


    // let generalIndex, scheduleIndex;
    // tempTables.value.forEach((table, index) => {
    //   if(table.tableName == 'General Settings')
    //     generalIndex = index;

    //   if(table.tableName == 'schedule')
    //     scheduleIndex = index;
    // })

    // console.log('before',this.dataForm.get('tempTables').value[generalIndex])
    // let x = this.dataForm.get('tempTables').value[0]
    // this.dataForm.get('tempTables').value[0] = this.dataForm.get('tempTables').value[generalIndex]
    // this.dataForm.get('tempTables').value[generalIndex] = x;
    // console.log('after',this.dataForm.get('tempTables').value[generalIndex])
    // console.log(this.dataForm.get('tempTables'))
    // console.log(this.dataForm)

    if (tempTables.value[1].defaultValue[0] === 'SCHEDULED') {
      scheduleForm.controls['cronExpression'].patchValue(tempTables.value[1].defaultValue[1]);
      scheduleForm.controls['startDate'].patchValue(new Date(tempTables.value[1].defaultValue[2]));
      scheduleForm.controls['endDate'].patchValue(new Date(tempTables.value[1].defaultValue[3]));
      this.startDate = scheduleForm.controls['startDate'].value;
    }

    if (this.schemaAvailable) {
      this.schemaString = tempTables.value.slice(-1)[0].defaultValue[0].split(',');
    }

  }

  defaultValueUpdate(fieldIndex, tableIndex, event) {
    const tempTables = this.dataForm.get('tempTables') as FormArray;
    tempTables.value[tableIndex].defaultValue[fieldIndex] = event.target.value;
  }

  scheduleValueUpdate(fieldIndex, tableIndex, event) {
    const formattedDate: string = new Date(event.value).toISOString();
    const tempTables = this.dataForm.get('tempTables') as FormArray;
    tempTables.value[tableIndex].defaultValue[fieldIndex] = formattedDate;
    this.startDate = this.dataForm.controls['scheduleForm'].value['startDate'];
  }

  getArray(num) {
    return num > 0 ? new Array(num).fill(0) : null;
  }

  validateReadOnly(fieldName) {
    if (fieldName === 'File Name' || fieldName === 'DB Name' || fieldName === 'Data Format'
      || fieldName === 'File Directory' || fieldName === 'API URL') {
      return false;
    } else {
      return true;
    }
  }

  uploadConfig() {
    const tempTables = this.dataForm.get('tempTables') as FormArray;
    tempTables.value[1].defaultValue[1] = this.dataForm.controls['scheduleForm'].value['cronExpression'];


    this.finalObject = this.job;
    this.finalObject.properties = [];

    Object.entries(tempTables.value).forEach((row, index) => {
      for (let i = 0; i < row[1]['propertiesNumber']; i++) {
        // if (row[1]['fieldName'][i] === 'startDate') { console.log('here')
        //   // tslint:disable-next-line: max-line-length
        //   this.finalObject.properties.push({ 'key': row[1]['fieldName'][i], 'value': row[1]['defaultValue'][i], 'category': row[1]['tableName'] });
        // }
        // tslint:disable-next-line: max-line-length
        this.finalObject.properties.push({ 'id': row[1]['fieldID'][i], 'key': row[1]['fieldName'][i], 'value': row[1]['defaultValue'][i], 'category': row[1]['tableName'] });
      }
    });


    // console.log('initial', this.job);
    // console.log('finalbject:', this.finalObject);

    // finalObject to submitted to api call
    // this.job.flowId for job id
    this.historyServcice.editJobUploadConfig(this.finalObject.flowId, this.finalObject).subscribe((data) => {
      // console.log(data);
      // console.log('flowId', this.finalObject.flowId);
      // console.log('finalObject', this.finalObject);
      this._snackBar.open('Job Edited Successfully.', 'Close', {
        duration: 3000,
      });
    },
      (error) => {
        this._snackBar.open('Failed to Edit Job', 'Close', {
          duration: 3000,
        });
      });

  }
}

