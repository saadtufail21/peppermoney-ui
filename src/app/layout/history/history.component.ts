import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HistoryService } from '../../services/history.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

import { Router } from '@angular/router';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {



  displayedColumns: string[] = [
    'country',
    'sourcesystem',
    'sourcetable',
    'source',
    'destination',
    'jobCreatedDate',
    'jobLastRunDate',
    'type',
    'active',
    'toggle',
    'action'
  ];
  dataSource: any;
  JOBMANAGEMENT_DATAAPI: any = [];

  // CSS Spinner Variable
  loaderDiv: Boolean = false;


  // For Hardcoded Data
  // dataSource = new MatTableDataSource<JobManagementElement>(JOBMANAGEMENT_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _snackBar: MatSnackBar,
    private historyServcice: HistoryService,
    private dialog: MatDialog,
    private router: Router,
  ) { }



  ngOnInit() {
    this.getalljobs();
  }


  getalljobs() {
    this.historyServcice.getAllJobs().subscribe(
      data => {
        console.log(data);
        this.JOBMANAGEMENT_DATAAPI = data;
        // For API
        this.dataSource = new MatTableDataSource<JobManagementElement>(this.JOBMANAGEMENT_DATAAPI);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this._snackBar.open('Data Loaded Successfully', 'Close', {
          duration: 3000,
        });
      },
      error => {
        this._snackBar.open('Couldn\'t load Data.', 'Close', {
          duration: 3000,
        });
      }
    );
  }

  // Testing for Mat-Dialog
  updateActiveStatus(element) {
    if (element.active = !element.active) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: 'Are you sure you want to Enable Job ?'
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === true) {
          // console.log('API TO ENABLE JOB EXECUTION');
          this.loaderDiv = true;

          this.historyServcice.startRescheduleJob(element.processGroupId).subscribe(
            data => {
              this.loaderDiv = false;
              this._snackBar.open('Job Scheduled Successfully.', 'Close', {
                duration: 3000,
              });

            },
            (error) => {
              element.active = !element.active;
              element.active = !element.active;
              this.loaderDiv = false;
              this._snackBar.open('Failed to Schedule Job', 'Close', {
                duration: 3000,
              });
            });
        } else {
          element.active = !element.active;
          // console.log('DO NOTHING');
        }
      });
    } else {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: 'Are you sure you want to Disable Job ?'
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === true) {
          // console.log('API TO DISBALE JOB EXECUTION');
          this.historyServcice.stopRescheduleJob(element.processGroupId).subscribe(
            data => {
              this._snackBar.open('Scheduled Job Paused Successfully.', 'Close', {
                duration: 3000,
              });
            }, (error) => {
              element.active = !element.active;
              this._snackBar.open('Failed Pause Scheduled Job', 'Close', {
                duration: 3000,
              });
            }
          );
        } else {
          element.active = !element.active;
          // console.log('DO NOTHING');
        }
      });
    }

  }


  // Execute Job
  executeJob(element: any) {


    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: 'Are you sure you want to Execute Job ?'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.loaderDiv = true;
        // console.log('element', element);
        // console.log('element.flowId', element.flowId);
        const finalObjectJSON: any[] = this.dataSource.filteredData;
        // console.log('finalObjectJSON', finalObjectJSON);
        const filteredflowdata = finalObjectJSON.filter(flowdata => {
          // if (flowdata.flowId === element.flowId) {
          if (flowdata.flowId === element.flowId) {
            return flowdata;
          }
        });
        // console.log('filteredflowdata', filteredflowdata);

        // This code converts filteredflowdata Array Object to just an Object.
        const array = filteredflowdata;

        const object_needed = array.find(d => d.flowId === filteredflowdata[0].flowId);
        // console.log(object_needed);

        // console.log('Country Code in Component', object_needed.properties[0].value);
        sessionStorage.setItem('countryCode', object_needed.properties[0].value);


        // Execute the Job
        this.historyServcice.adhocJob(element.processGroupId, object_needed)
          .subscribe(
            (data) => {
              // console.log(data);
              this.loaderDiv = false;
              this._snackBar.open('Job Registered Successfully for Execution.', 'Close', {
                duration: 3000,
              });
            },
            (error) => {
              this.loaderDiv = false;
              this._snackBar.open('Failed to register Job for Execution.', 'Close', {
                duration: 3000,
              });
            }
          );
      }
    });



  }

  // Edit Job
  editJob(element) {
    sessionStorage.setItem('editProcessGroupId', element.processGroupId);
    // console.log('History Page: element.processGroupId', element.processGroupId);
    this.router.navigateByUrl('/edit-job');
  }

  // Delete Job
  deleteJob(element: any) {
    const finalObjectJSON: any[] = this.dataSource.filteredData;
    // console.log('finalObjectJSON', finalObjectJSON);
    const filteredflowdata = finalObjectJSON.filter(flowdata => {
      // if (flowdata.flowId === element.flowId) {
      if (flowdata.flowId === element.flowId) {
        return flowdata;
      }
    });
    // console.log('filteredflowdata', filteredflowdata);

    const jobType = filteredflowdata.filter(jobType => {
      // if (flowdata.flowId === element.flowId) {
      if (jobType.active === true) {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
          data: 'Job is scheduled for Execution,Are you sure you want to delete it?'
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result === true) {
            // console.log('Delete Button is working');
            // console.log('element', element);
            // console.log('element.flowId', element.flowId);
            // console.log('API TO DISBALE JOB EXECUTION');
            this.historyServcice.stopRescheduleJob(element.processGroupId).subscribe(
              data => {
                // Delete Job after Unscheduling.
                this.historyServcice.deleteJob(element.processGroupId).subscribe((data) => {
                  // console.log(data);
                  this._snackBar.open('Job Deleted Successfully.', 'Close', {
                    duration: 3000,
                  });
                  this.dataSource = new MatTableDataSource<JobManagementElement>(this.JOBMANAGEMENT_DATAAPI);
                },
                  (error) => {
                    this._snackBar.open('Failed to Delete Job', 'Close', {
                      duration: 3000,
                    });
                  });
              }
            );

          }
        });
      } else {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
          data: 'Are you sure you want to delete ?'
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result === true) {
            // console.log('Delete Button is working');
            // console.log('element', element);
            // console.log('element.flowId', element.flowId);


            // Delete Job after Unscheduling.
            this.historyServcice.deleteJob(element.processGroupId).subscribe((data) => {
              // console.log(data);
              this._snackBar.open('Job Deleted Successfully.', 'Close', {
                duration: 3000,
              });
              this.dataSource = new MatTableDataSource<JobManagementElement>(this.JOBMANAGEMENT_DATAAPI);
            },
              (error) => {
                this._snackBar.open('Failed to Delete Job', 'Close', {
                  duration: 3000,
                });
              });
          }
        });
      }


    });
  }




}


export interface JobManagementElement {
  flowId: number;
  flowName: string;
  processGroupId: string;
  oozieJobId: string;
  version: number;
  active: boolean;
  jobCreatedDate: string;
  jobUpdateDate: string;
  jobLastRunDate: string;
  userId: string;
  properties: Array<Properties>;
  // activated: true;  For toggle button.
}
export interface Properties {
  id: number;
  key: string;
  value: string;
  category: any;
}
// To be used if API is not working
// const JOBMANAGEMENT_DATA: Array<JobManagementElement> =
//   [
//     {
//       'flowId': 6149,
//       'flowName': 'jackson-demo_delta_with_headers',
//       'version': 1,
//       'processGroupId': '314ec3a6-0171-1000-d89f-f70b62a8d949',
//       'jobCreatedDate': '2020-03-29',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000353-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 6150,
//           'key': 'country',
//           'value': 'au',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6151,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6152,
//           'key': 'sourceTable',
//           'value': 'demo_delta_with_headers',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6153,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6154,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6155,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 6156,
//           'key': 'Source',
//           'value': 'hive',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6157,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6158,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6171,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6159,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6160,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6161,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6162,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6163,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6164,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6165,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6166,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6167,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6168,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6169,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6170,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6172,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6173,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6174,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6175,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6176,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6177,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6178,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6179,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6180,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6181,
//           'key': 'Header Present',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6182,
//           'key': 'Enable SCD',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6183,
//           'key': 'SCD Type',
//           'value': 'delta',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6184,
//           'key': 'Key Column',
//           'value': 'id',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6185,
//           'key': 'Contains IUD Flag',
//           'value': 'true',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6186,
//           'key': 'Insert Flag',
//           'value': 'i',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6187,
//           'key': 'Delete Flag',
//           'value': 'd',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6188,
//           'key': 'Update Flag',
//           'value': 'u',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6189,
//           'key': 'Flag Column',
//           'value': 'flag',
//           'category': 'SCD Details'
//         }
//       ]
//     },
//     {
//       'flowId': 5659,
//       'flowName': 'jackson-demo_incremental_with_header',
//       'version': 1,
//       'processGroupId': '2fe95254-0171-1000-20af-d3f57ab36897',
//       'jobCreatedDate': '2020-03-28',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000339-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 5660,
//           'key': 'country',
//           'value': 'in',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5661,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5662,
//           'key': 'sourceTable',
//           'value': 'demo_incremental_with_header',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5663,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5664,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5665,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 5666,
//           'key': 'Source',
//           'value': 'hbase',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5667,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5668,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5669,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5670,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5671,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5672,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5673,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5674,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5675,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5676,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5677,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5678,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5679,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5680,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5681,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5682,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5683,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5684,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5685,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5686,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5687,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5688,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5689,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5690,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5691,
//           'key': 'Header Present',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5692,
//           'key': 'Enable SCD',
//           'value': 'false',
//           'category': 'Destination Properties'
//         }
//       ]
//     },
//     {
//       'flowId': 6231,
//       'flowName': 'jackson-demo_delta_without_iud_flags',
//       'version': 1,
//       'processGroupId': '315d2cfd-0171-1000-41f5-cb9b3a94ded4',
//       'jobCreatedDate': '2020-03-27',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000356-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 6232,
//           'key': 'country',
//           'value': 'ksa',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6233,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6234,
//           'key': 'sourceTable',
//           'value': 'demo_delta_without_iud_flags',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6235,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6236,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6237,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 6238,
//           'key': 'Source',
//           'value': 'aws',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6239,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6240,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6241,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6242,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6243,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6244,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6245,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6246,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6247,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6248,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6249,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6250,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6251,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6252,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6253,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6254,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6255,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6256,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6257,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6258,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6259,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6260,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6261,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6262,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6263,
//           'key': 'Header Present',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6264,
//           'key': 'Enable SCD',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6265,
//           'key': 'SCD Type',
//           'value': 'delta',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6266,
//           'key': 'Key Column',
//           'value': 'id',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6267,
//           'key': 'Contains IUD Flag',
//           'value': 'false',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6268,
//           'key': 'Insert Flag',
//           'value': 'i',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6269,
//           'key': 'Delete Flag',
//           'value': 'd',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6270,
//           'key': 'Update Flag',
//           'value': 'u',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6271,
//           'key': 'Flag Column',
//           'value': 'flag',
//           'category': 'SCD Details'
//         }
//       ]
//     },
//     {
//       'flowId': 5693,
//       'flowName': 'jackson-demo_incremental_without_header',
//       'version': 1,
//       'processGroupId': '2fea1a8b-0171-1000-4db6-3d028756e66d',
//       'jobCreatedDate': '2020-03-26',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000340-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 5694,
//           'key': 'country',
//           'value': 'uae',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5695,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5696,
//           'key': 'sourceTable',
//           'value': 'demo_incremental_without_header',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5697,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5698,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5699,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 5700,
//           'key': 'Source',
//           'value': 's3',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5701,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5702,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5703,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5704,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5705,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5706,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5707,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5708,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5709,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5710,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5711,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5712,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5713,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5714,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5715,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5716,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5717,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5718,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5719,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5720,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5721,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5722,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5723,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5724,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5725,
//           'key': 'Header Present',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5726,
//           'key': 'Enable SCD',
//           'value': 'false',
//           'category': 'Destination Properties'
//         }
//       ]
//     },
//     {
//       'flowId': 5727,
//       'flowName': 'jackson-demo_full_snapshot_with_schema',
//       'version': 1,
//       'processGroupId': '302f8b75-0171-1000-dcaf-03322b58a1e3',
//       'jobCreatedDate': '2020-03-25',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000342-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 5728,
//           'key': 'country',
//           'value': 'usa',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5729,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5730,
//           'key': 'sourceTable',
//           'value': 'demo_full_snapshot_with_schema',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5731,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5732,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5733,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 5734,
//           'key': 'Source',
//           'value': 'redshift',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5735,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5736,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5737,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5738,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5739,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5740,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5741,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5742,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5743,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5744,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5745,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5746,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5747,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5748,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5749,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5750,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5751,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5752,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5753,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5754,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5755,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5756,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5757,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5758,
//           'key': 'Schema Available',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5759,
//           'key': 'Header Present',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5760,
//           'key': 'Enable SCD',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5761,
//           'key': 'schema',
//           'value': 'id:int,name:string,age:int,date:date,address:string',
//           'category': 'Schema'
//         }
//       ]
//     },
//     {
//       'flowId': 5762,
//       'flowName': 'jackson-demo_full_snapshot_with_header',
//       'version': 1,
//       'processGroupId': '303ada83-0171-1000-c196-d621854a1daf',
//       'jobCreatedDate': '2020-03-24',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000344-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 5763,
//           'key': 'country',
//           'value': 'ger',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5764,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5765,
//           'key': 'sourceTable',
//           'value': 'demo_full_snapshot_with_header',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5766,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5767,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 5768,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 5769,
//           'key': 'Source',
//           'value': 'mysql',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5770,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5771,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5772,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5773,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5774,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5775,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5776,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5777,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5778,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5779,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5780,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 5781,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5782,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5783,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5784,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5785,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5786,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5787,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5788,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5789,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5790,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5791,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5792,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5793,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5794,
//           'key': 'Header Present',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 5795,
//           'key': 'Enable SCD',
//           'value': 'false',
//           'category': 'Destination Properties'
//         }
//       ]
//     },
//     {
//       'flowId': 6051,
//       'flowName': 'jackson-demo_full_snapshot_without_headers',
//       'version': 1,
//       'processGroupId': '310fa56a-0171-1000-0499-5148bbb618a7',
//       'jobCreatedDate': '2020-03-23',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000349-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 6052,
//           'key': 'country',
//           'value': 'swe',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6053,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6054,
//           'key': 'sourceTable',
//           'value': 'demo_full_snapshot_without_headers',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6055,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6056,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6057,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 6058,
//           'key': 'Source',
//           'value': 'oracle',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6059,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6060,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6061,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6062,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6063,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6064,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6065,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6066,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6067,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6068,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6069,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6070,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6071,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6072,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6073,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6074,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6075,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6076,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6077,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6078,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6079,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6080,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6081,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6082,
//           'key': 'Schema Available',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6083,
//           'key': 'Header Present',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6084,
//           'key': 'Enable SCD',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6085,
//           'key': 'SCD Type',
//           'value': 'full-snapshot',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6086,
//           'key': 'Key Column',
//           'value': 'id',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6087,
//           'key': 'Contains IUD Flag',
//           'value': 'false',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6088,
//           'key': 'Insert Flag',
//           'value': 'i',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6089,
//           'key': 'Delete Flag',
//           'value': 'd',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6090,
//           'key': 'Update Flag',
//           'value': 'u',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6091,
//           'key': 'Flag Column',
//           'value': 'flag',
//           'category': 'SCD Details'
//         }
//       ]
//     },
//     {
//       'flowId': 6092,
//       'flowName': 'jackson-demo_delta_with_schemas',
//       'version': 1,
//       'processGroupId': '3133f86c-0171-1000-8138-ea84013550ee',
//       'jobCreatedDate': '2020-03-22',
//       'jobUpdateDate': '',
//       'jobLastRunDate': '2020-03-31',
//       'oozieJobId': '0000351-200218064327501-oozie-oozi-W',
//       'active': false,
//       'userId': 'sa_aad_abaldua',
//       'properties': [
//         {
//           'id': 6093,
//           'key': 'country',
//           'value': 'in',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6094,
//           'key': 'sourceSystem',
//           'value': 'jackson',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6095,
//           'key': 'sourceTable',
//           'value': 'demo_delta_with_schemas',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6096,
//           'key': 'template',
//           'value': 'adls-to-hive',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6097,
//           'key': 'lineageMode',
//           'value': 'File',
//           'category': 'General Settings'
//         },
//         {
//           'id': 6098,
//           'key': 'scheduleType',
//           'value': 'ADHOC',
//           'category': 'schedule'
//         },
//         {
//           'id': 6099,
//           'key': 'Source',
//           'value': 'amazon',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6100,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6101,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6102,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6103,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6104,
//           'key': 'Source File Directory',
//           'value': '',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6105,
//           'key': 'File Name',
//           'value': '^.*emp_data.*$',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6106,
//           'key': 'Key Vault Url',
//           'value': 'https://pepgdl-kms-vault.vault.azure.net/',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6107,
//           'key': 'Secret Identifier',
//           'value': 'AesKey',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6108,
//           'key': 'Key Identifier',
//           'value': 'PepperKeyRSA',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6109,
//           'key': 'Source Layer',
//           'value': 'landing',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6110,
//           'key': 'Source Path',
//           'value': '/landing/in/jackson',
//           'category': 'Source Properties'
//         },
//         {
//           'id': 6111,
//           'key': 'Destination',
//           'value': 'hive',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6112,
//           'key': 'Hive Table Type',
//           'value': 'external',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6113,
//           'key': 'Client ID',
//           'value': 'ebf73456-443e-4986-941b-057906d25e2f',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6114,
//           'key': 'Client Key',
//           'value': 'Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6115,
//           'key': 'Auth Token Endpoint',
//           'value': 'https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6116,
//           'key': 'Account FQDN',
//           'value': 'pepperadlsuat.azuredatalakestore.net',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6117,
//           'key': 'Destination File Directory',
//           'value': '',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6118,
//           'key': 'Data Format',
//           'value': 'csv',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6119,
//           'key': 'Field Delimiter',
//           'value': ',',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6120,
//           'key': 'DB Name',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6121,
//           'key': 'Target Layer',
//           'value': 'raw',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6122,
//           'key': 'Target Path',
//           'value': '/raw/in/jackson',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6123,
//           'key': 'Schema Available',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6124,
//           'key': 'Header Present',
//           'value': 'false',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6125,
//           'key': 'Enable SCD',
//           'value': 'true',
//           'category': 'Destination Properties'
//         },
//         {
//           'id': 6126,
//           'key': 'SCD Type',
//           'value': 'delta',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6127,
//           'key': 'Key Column',
//           'value': 'id',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6128,
//           'key': 'Contains IUD Flag',
//           'value': 'true',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6129,
//           'key': 'Insert Flag',
//           'value': 'i',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6130,
//           'key': 'Delete Flag',
//           'value': 'd',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6131,
//           'key': 'Update Flag',
//           'value': 'u',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6132,
//           'key': 'Flag Column',
//           'value': 'flag',
//           'category': 'SCD Details'
//         },
//         {
//           'id': 6133,
//           'key': 'schema',
//           'value': 'id:int,name:string,age:int,date:date,address:string,flag:string',
//           'category': 'Schema'
//         }
//       ]
//     }
//   ];


