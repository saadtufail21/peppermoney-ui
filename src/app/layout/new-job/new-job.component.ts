import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { TemplateService } from '../../services/template.service';
import { JobService } from '../../services/job.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CronOptions } from 'cron-editor/lib/CronOptions';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.scss']
})
export class NewJobComponent implements OnInit {
  // @OUTPUT sends finalObject JSON to History Page.
  @Output() getFinalObjectJSON: EventEmitter<any> = new EventEmitter();

  // country = ['au', 'kr', 'gb', 'in'];
  country: any = [];
  // sourceSystem = ['jackson', 'hive_testing', 'ui_testing'];
  sourceSystem: any = [];
  // lineageMode = ['File', 'Folder'];
  lineageMode: any = [];
  // templateList: any = ['l'];
  templateList: any = [];
  generalInfoForm: FormGroup;
  scheduleForm: FormGroup;
  scdGroup: FormGroup;
  tempTables = [];
  schedule = false;
  finalTables = [];
  finalObject;
  jobID;
  enableExecution = false;
  enableUpload = false;
  enableSCD;
  schemaAvailable;
  countryValue: string;
  selectedTemplate: String;
  schemaForm: FormGroup;
  minimumSchema = 1;
  schemaDataTypes = ['null', 'boolean', 'int', 'long', 'float', 'double', 'bytes', 'string', 'date', 'timestamp'];
  schemaStringOption = true;
  schemaStringValue;
  scdFields = ['Insert Flag', 'Update Flag', 'Delete Flag', 'Flag Column'];
  scdContainsIUD = true;
  ddlObject = [];

  // finalObject: FinalModel;
  // CSS Spinner Variable
  loaderDiv: Boolean = false;
  // UTC Time on Schedule Variables
  timer;
  time = new Date();
  str = new Date().toUTCString();

  cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',
    defaultTime: '10:00:00',
    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,
    use24HourTime: true,
    hideSeconds: false,
    removeSeconds: false,
    removeYears: false,
  };
  cronGroup: FormGroup;

  utcTime = new Date().toUTCString();
  getallattribute; // Store whole JSON.

  constructor(
    private templateService: TemplateService,
    private jobservice: JobService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private adminService: AdminService
  ) { }


  ngOnInit() {

    this.getAllDropdownValues();
    this.getTemplateList();

    // Timer to Display UTC time on schedule
    this.timer = setInterval(() => {
      this.str = new Date().toUTCString();
      this.time = new Date();
    }, 1000);

    this.cronGroup = new FormGroup({
      cronExpression: new FormControl('0 0 1/1 * *', [Validators.required]),
      startDate: new FormControl('', [Validators.required]),
      endDate: new FormControl('', [Validators.required])
    });

    this.generalInfoForm = new FormGroup({
      country: new FormControl('', [Validators.required]),
      sourceSystem: new FormControl('', [Validators.required]),
      sourceTable: new FormControl('', [Validators.required]),
      template: new FormControl('', [Validators.required]),
      lineageMode: new FormControl('', [Validators.required]),
      scheduled: new FormControl({ disable: true }),
    });

    this.schemaForm = new FormGroup({
      schemaArray: new FormArray([])
    });
  }

  // Get all Dropdown Values
  getAllDropdownValues() {
    // Get dropdown Values
    this.adminService.getAllAttributes().subscribe((data) => {
      // console.log(data);
      this.getallattribute = data;
      // console.log(this.getallattribute);


      // Get Country DropDown List
      const countryArray = this.getallattribute.filter(element => element.attributeName === 'country');
      // console.log(countryArray);
      this.adminService.getAttributesValues(countryArray[0].attId).subscribe((data) => {
        this.getallattribute = data;
        // Test -1
        for (const [key, value] of Object.entries(data)) {
          if (key == 'attributeValues') {
            this.country = value;
            // console.log(this.country);
            // console.log('Test -1', key, value);
          }
        }
      });

      // Code for   Source System Dropdown
      const sourceSystemArray = this.getallattribute.filter(element => element.attributeName === 'sourceSystem');
      // console.log(sourceSystemArray);
      this.adminService.getAttributesValues(sourceSystemArray[0].attId).subscribe((data) => {
        this.getallattribute = data;
        // Test -1
        for (const [key, value] of Object.entries(data)) {
          if (key == 'attributeValues') {
            this.sourceSystem = value;
            // console.log(this.sourceSystem);
            // console.log('Test -1', key, value);
          }
        }
      });
      // Code for Lineage Dropdown
      const lineageModeArray = this.getallattribute.filter(element => element.attributeName === 'lineageMode');
      // console.log(lineageModeArray);
      this.adminService.getAttributesValues(lineageModeArray[0].attId).subscribe((data) => {
        this.getallattribute = data;
        // Test -1
        for (const [key, value] of Object.entries(data)) {
          if (key == 'attributeValues') {
            this.lineageMode = value;
            // console.log(this.lineageMode);
            // console.log('Test -1', key, value);
          }
        }
      });
    });
  }

  getArray(num) {
    return num > 0 ? new Array(num).fill(0) : null;
  }

  getTemplateList() {
    this.jobservice.getTemplateByTemplateList().subscribe(
      data => {
        // console.log(data);
        this.templateList = data;
      },
      error => {
        // console.log('Could not call API.');
      }
    );
  }

  loadTemplate() {
    this.jobservice.getTemplateByTemplateName(this.selectedTemplate).subscribe(
      data => {
        this.tempTables = data['tempTables'];
        this.formatDefaultValues();
        this.formatExtraTables();
        this.formatDDLObject();

        this.finalTables = JSON.parse(JSON.stringify(this.tempTables));
        this.enableUpload = true;

        this._snackBar.open('Template Loaded Successfully.', 'Close', {
          duration: 3000,
        });
      },
      error => {
        // console.log('Could not call API.');
        this._snackBar.open('Failed to Load Template Data.', 'Close', {
          duration: 3000,
        });
      }
    );
  }

  //   let data = {
  //     "templateId" : 1861,
  //     "templateName" : "adls-to-hive",
  //     "tempTables" : [ {
  //       "tempTableId" : 1862,
  //       "tableName" : "Source Properties",
  //       "propertiesNumber" : 12,
  //       "fieldName" : [ "Source", "Client ID", "Client Key", "Auth Token Endpoint", "Account FQDN", "Source File Directory", "File Name", "Key Vault Url", "Secret Identifier", "Key Identifier", "Source Layer", "Source Path" ],
  //       "dataType" : [ "DropDown List", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text" ],
  //       "defaultValue" : [ "adls, koko, soso, lolo", "ebf73456-443e-4986-941b-057906d25e2f", "Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t", "https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token", "pepperadlsuat.azuredatalakestore.net", "", "^.*regex.*$", "https://pepgdl-kms-vault.vault.azure.net/", "AesKey", "PepperKeyRSA", "landing", "" ]
  //     }, {
  //       "tempTableId" : 1863,
  //       "tableName" : "Destination Properties",
  //       "propertiesNumber" : 15,
  //       "fieldName" : [ "Destination", "Hive Table Type", "Client ID", "Client Key", "Auth Token Endpoint", "Account FQDN", "Destination File Directory", "Data Format", "Field Delimiter", "DB Name", "Target Layer", "Target Path", "Schema Available", "Header Present", "Enable SCD" ],
  //       "dataType" : [ "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "Text", "CheckBox", "CheckBox", "CheckBox" ],
  //       "defaultValue" : [ "hive", "external", "ebf73456-443e-4986-941b-057906d25e2f", "Ano-Id0bG/Sx2J17:.q@?jBjZ0DLn4.t", "https://login.microsoftonline.com/0eab9659-aad3-462e-bfd8-c42856b65c6f/oauth2/token", "pepperadlsuat.azuredatalakestore.net", "", "csv", "|", "raw", "raw", "", "false", "true", "true" ]
  //     }, {
  //       "tempTableId" : 1864,
  //       "tableName" : "SCD Details",
  //       "propertiesNumber" : 7,
  //       "fieldName" : [ "SCD Type", "Key Column", "Contains IUD Flag", "Insert Flag", "Delete Flag", "Update Flag", "Flag Column"],
  //       "dataType" : [ "DropDown List", "Text", "CheckBox", "Text", "Text", "Text", "Text" ],
  //       "defaultValue" : [ "delta,full snap-shot", "id", "true", "i", "d", "u", "flag" ]
  //     } ]
  //   }

  //   this.tempTables = data['tempTables']
  //   console.log(this.tempTables)

  //   this.formatDefaultValues()
  //   this.formatExtraTables()
  //   this.formatDDLObject()
  //   this.finalTables = JSON.parse(JSON.stringify(this.tempTables));
  //   this.enableUpload = true;

  //   console.log(this.tempTables)
  // }

  formatDefaultValues() {
    this.tempTables.forEach((table, index) => {
      table.fieldName.forEach((field, fieldIndex) => {
        if (field === 'Base File Directory') {
          let str = '/landing/';
          str = str.concat(this.generalInfoForm.get('country').value, '/', this.generalInfoForm.get('sourceSystem').value);
          table.defaultValue[fieldIndex] = str.toLowerCase();
        }
        if (field === 'Table Name') {
          const str = this.generalInfoForm.get('sourceTable').value;
          table.defaultValue[fieldIndex] = str.toLowerCase();
        }
        if (field === 'Source Path') {
          let str = '/' + table.defaultValue[table.fieldName.indexOf('Source Layer')];
          str = str.concat('/', this.generalInfoForm.get('country').value, '/', this.generalInfoForm.get('sourceSystem').value);
          table.defaultValue[fieldIndex] = str.toLowerCase();
        }
        if (field === 'Target Path') {
          let str = '/' + table.defaultValue[table.fieldName.indexOf('Target Layer')];
          str = str.concat('/', this.generalInfoForm.get('country').value, '/', this.generalInfoForm.get('sourceSystem').value);
          table.defaultValue[fieldIndex] = str.toLowerCase();
        }
      });
    });
  }

  // Old Code
  // formatExtraTables() {
  //   this.tempTables.forEach((table, index) => {
  //     if (table.fieldName.indexOf('Enable SCD') > -1) {
  //       this.enableSCD = table.defaultValue[table.fieldName.indexOf('Enable SCD')] ;
  //     }
  //     if (table.fieldName.indexOf('Schema Available') > -1) {
  //       this.schemaAvailable = table.defaultValue[table.fieldName.indexOf('Schema Available')];
  //       const schemaArray = this.schemaForm.get('schemaArray') as FormArray;
  //       schemaArray.push(this.formBuilder.group({
  //         columnName: ['', [Validators.required]],
  //         dataType: ['', [Validators.required]]
  //       }));
  //     }
  //   });
  // }

  // New Code

  formatExtraTables() {
    this.tempTables.forEach((table, index) => {
      if (table.fieldName.indexOf('Enable SCD') > -1) {
        this.enableSCD = table.defaultValue[table.fieldName.indexOf('Enable SCD')];
      }
      if (table.fieldName.indexOf('Schema Available') > -1) {
        this.schemaAvailable = table.defaultValue[table.fieldName.indexOf('Schema Available')];
        table.defaultValue[table.fieldName.indexOf('Schema Available')] == 'true' ? this.createSchemaItems({ target: { value: 1, valueAsNumber: 1 } }) : '';
      }
    });
  }

  formatDDLObject() {
    this.tempTables.forEach((table) => {
      table.dataType.forEach((type, index) => {
        if (type == 'DropDown List') {
          const valueArray = table.defaultValue[index].split(',');
          table.defaultValue[index] = valueArray;

          this.ddlObject.push({
            'attributeName': table.fieldName[index],
            'attributeValues': valueArray
          });
        }
      });
    });

    // console.log(this.ddlObject);
  }

  defaultValueUpdate(fieldIndex, tableIndex, event, fieldName) {
    event.target ?
      this.finalTables[tableIndex].defaultValue[fieldIndex] = event.target.value :
      // event.checked ?
      event.checked != null ?
        this.finalTables[tableIndex].defaultValue[fieldIndex] = event.checked.toString() :
        this.finalTables[tableIndex].defaultValue[fieldIndex] = event.value

    fieldName === 'Enable SCD' ? this.enableSCD = event.checked.toString() : '';

    if (fieldName === 'Schema Available') {
      this.schemaAvailable = event.checked.toString();
      const schemaArray = this.schemaForm.get('schemaArray') as FormArray;
      this.schemaAvailable === 'false' ? schemaArray.clear() :
        schemaArray.push(this.formBuilder.group({
          columnName: ['', [Validators.required]],
          dataType: ['string', [Validators.required]]
        }));
    }
  }

  isSCDField(fieldName) {
    if (this.scdFields.includes(fieldName)) {
      return 'scdElement';
    }
  }

  toggleSCDItems(fieldIndex, tableIndex, event, fieldName) {
    let style;
    this.scdContainsIUD = event.checked;
    event.checked ? style = 'block' : style = 'none'
    const scdItems = document.getElementsByClassName('scdElement');
    Object.keys(scdItems).forEach(element => {
      scdItems[element].style.display = style;
    });
    this.defaultValueUpdate(fieldIndex, tableIndex, event, fieldName);
  }

  schemaDataTypeUpdate(fieldIndex, event) {
    const schemaArray = this.schemaForm.get('schemaArray') as FormArray;
    schemaArray.value[fieldIndex].dataType = event.value;
  }

  createSchemaItems(event) {
    const schemaArray = this.schemaForm.get('schemaArray') as FormArray;
    schemaArray.length > event.target.value ?
      this.getArray(schemaArray.length - event.target.valueAsNumber).forEach((val, index) => {
        schemaArray.removeAt(schemaArray.length - 1 - index);
      }) :
      this.getArray(event.target.valueAsNumber - schemaArray.length).forEach(() => {
        schemaArray.push(this.formBuilder.group({
          columnName: ['', [Validators.required]],
          dataType: ['string', [Validators.required]]
        }));
      });
  }

  uploadConfig() {
    this.loaderDiv = true;
    this.finalObject = {
      flowName: this.generalInfoForm.get('sourceTable').value,
      version: '',
      processGroupID: '',
      jobCreatedDate: '',
      jobUpdateDate: '',
      jobLastRunDate: '',
      active: '',
      properties: []
    };

    // General properties
    Object.entries(this.generalInfoForm.value).forEach((row, index) => {
      index < 5 ? this.finalObject.properties.push({ 'key': row[0], 'value': row[1], 'category': 'General Settings' }) : '';
    });
    // Schedule properties
    if (this.schedule) {
      const formattedStartDate: string = new Date(this.cronGroup.get('startDate').value).toISOString();
      const formattedEndDate: string = new Date(this.cronGroup.get('endDate').value).toISOString();

      this.finalObject.properties.push({ 'key': 'scheduleType', 'value': 'SCHEDULED', 'category': 'schedule' });
      this.finalObject.properties.push({ 'key': 'cron', 'value': this.cronGroup.get('cronExpression').value, 'category': 'schedule' });
      this.finalObject.properties.push({ 'key': 'startDate', 'value': formattedStartDate, 'category': 'schedule' });
      this.finalObject.properties.push({ 'key': 'endDate', 'value': formattedEndDate, 'category': 'schedule' });
    } else {
      this.finalObject.properties.push({ 'key': 'scheduleType', 'value': 'ADHOC', 'category': 'schedule' });
    }
    // Additional properties
    Object.entries(this.finalTables).forEach((row, index) => {
      if (row[1].tableName === 'SCD Details') {
        if (this.enableSCD === 'true') {
          for (let i = 0; i < row[1]['propertiesNumber']; i++) {
            let defaultValue = row[1].defaultValue[i]
            Array.isArray(defaultValue) ? defaultValue = defaultValue[0] : ''
            this.finalObject.properties.push({ 'key': row[1].fieldName[i], 'value': defaultValue, 'category': row[1].tableName });
          }
        }
      } else {
        for (let i = 0; i < row[1]['propertiesNumber']; i++) {
          let defaultValue = row[1].defaultValue[i]
          Array.isArray(defaultValue) ? defaultValue = defaultValue[0] : ''
          this.finalObject.properties.push({ 'key': row[1].fieldName[i], 'value': defaultValue, 'category': row[1].tableName });
        }
      }
    });
    // Object.entries(this.finalTables).forEach((row, index) => {
    //   if (row[1].tableName === 'SCD Details') {
    //     if (this.enableSCD === 'true') {
    //       if (this.scdContainsIUD) {
    //         for (let i = 0; i < row[1]['propertiesNumber']; i++) {
    //           let defaultValue = row[1].defaultValue[i];
    //           Array.isArray(defaultValue) ? defaultValue = defaultValue[0] : '';
    //           this.finalObject.properties.push({ 'key': row[1].fieldName[i], 'value': defaultValue, 'category': row[1].tableName });
    //         }
    //       } else {
    //         for (let i = 0; i < row[1]['propertiesNumber'] && !this.isSCDField(row[1].fieldName[i]); i++) {
    //           let defaultValue = row[1].defaultValue[i];
    //           Array.isArray(defaultValue) ? defaultValue = defaultValue[0] : '';
    //           this.finalObject.properties.push({ 'key': row[1].fieldName[i], 'value': defaultValue, 'category': row[1].tableName });
    //         }
    //       }
    //     }
    //   } else {
    //     for (let i = 0; i < row[1]['propertiesNumber']; i++) {
    //       let defaultValue = row[1].defaultValue[i];
    //       Array.isArray(defaultValue) ? defaultValue = defaultValue[0] : '';
    //       this.finalObject.properties.push({ 'key': row[1].fieldName[i], 'value': defaultValue, 'category': row[1].tableName });
    //     }
    //   }
    // });

    // Schema Properties
    if (this.schemaAvailable === 'true') {
      let schemaString = '';
      if (this.schemaStringOption) {
        schemaString = this.schemaStringValue;
      } else {
        const schemaArray = this.schemaForm.get('schemaArray') as FormArray;
        schemaArray.value.forEach((row, index) => {
          index === schemaArray.value.length - 1 ?
            schemaString = schemaString.concat(row.columnName, ':', row.dataType) :
            schemaString = schemaString.concat(row.columnName, ':', row.dataType, ',');
        });
      }
      this.finalObject.properties.push({ 'key': 'schema', 'value': schemaString, 'category': 'Schema' });
    }

    this.getFinalObjectJSON.emit(this.finalObject);

    // console.log(JSON.stringify(this.finalObject.properties));

    // this is for rendering template
    // this.jobService.createJob(this.finalTables, this.generalInfoForm.get('template').value)


    // this is for upload config
    this.jobservice.uploadConfig(this.selectedTemplate, this.finalObject)
      .subscribe(
        (data) => {
          // console.log(data);
          this.jobID = data['response'];
          this.enableExecution = true;
          this.loaderDiv = false;
          this._snackBar.open('Job Created Successfully.', 'Close', {
            duration: 3000,
          });
        },
        (error) => {
          this.loaderDiv = false;
          this._snackBar.open('Failed to create Job.', 'Close', {
            duration: 3000,
          });
        }
      );
  }

  // // JobID is hard code

  // executeJob() {
  //   this.jobservice.adhocJob(1234, this.finalObject)
  //     .subscribe(
  //       (data) => {
  //         console.log(data);
  //       },
  //       (error) => {
  //       }
  //     );
  // }

  // This is to pass Country Code Value to Header in Service File

  onCountrySelection() {
    console.log(this.countryValue);
    sessionStorage.setItem('countryCode', this.countryValue);

  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }
}

// export interface FinalModel  {
//   flowName: string;
//   properties: PropertiesModel[];
// }
// export interface PropertiesModel {
//   key: string;
//   value: any;
//   categories: string;
// }
