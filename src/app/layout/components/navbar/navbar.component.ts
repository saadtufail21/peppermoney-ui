import { Component, OnInit, HostListener, ElementRef, AfterViewInit } from '@angular/core';
import { MainMenuService } from '../main-menu/main-menu.service';
import { Router } from '@angular/router';
import { SessionService } from '../../../services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public showMainMenu: boolean = false;
  currentUser: string;
  

  constructor(private elementRef: ElementRef,
    public serMainMenu: MainMenuService,
    private router: Router,
    private user: SessionService
  ) {
    user.getLoggedInName.subscribe(name => this.changeName(name));
  }


  private changeName(name: string): void {
    this.currentUser = name;
  }

  ngOnInit() {

  }

  // ngAfterViewInit() {
  //   this.currentUser = this.usernameTransfer.getData();
  //   console.log(this.currentUser);
  // }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    if (window.innerWidth >= 992) {
      this.showMainMenu = false;
      this.serMainMenu.hide();
    }
  }
  public mainMenu() {
    this.showMainMenu = !this.showMainMenu;
    this.serMainMenu.toggle();
  }
  signOut() {
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigateByUrl('/');
  }

}
