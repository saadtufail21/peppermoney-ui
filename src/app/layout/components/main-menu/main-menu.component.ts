import { Component, OnInit } from '@angular/core';
import { MainMenuService } from './main-menu.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  versionNumber = '0.4';
  constructor(public serMainMenu: MainMenuService) { }

  ngOnInit() {
  }

}
