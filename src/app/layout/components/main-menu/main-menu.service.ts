import { Injectable } from '@angular/core';

@Injectable()
export class MainMenuService {

  showMenu: boolean;

  constructor() { this.showMenu = false; }

  hide() { this.showMenu = false; }

  show() { this.showMenu = true; }

  toggle() { this.showMenu = !this.showMenu; }


}
