import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  getallattribute: any = []; // Store whole JSON.

  // To retrieve and Display Values
  allAttributes: string[] = []; // Store Values for 1st dropdown
  isSelected: Boolean = false;

  selectedAttribute; // Value for 1 Dropdown
  selectedAttributeValues: any; // To populate in html

  // To Add Values
  globalAttributeDataValue: string; // This holds the value for addValue function from onselectionchange
  newInputValue; // Value to set using ngModel.


  constructor(private adminService: AdminService, private _snackBar: MatSnackBar, ) { }

  ngOnInit() {
    // Get all attributes
    this.adminService.getAllAttributes().subscribe((data) => {

      this.getallattribute = data;
      // console.log('Get all Attributes: ', JSON.stringify(data));
      let size = Object.keys(this.getallattribute).length;
      // console.log('Length of Object: ', size);

      // Get List of All Attribute Values to display in First Dropdown
      for (const dataObject of this.getallattribute) {
        this.allAttributes.push(dataObject.attributeName);
        // console.log(dataObject);
        // console.log(dataObject.attributeName);
      }

    },
      (error) => {
        this._snackBar.open('Failed to fetch attributes', 'Close', {
          duration: 3000,
        });
      });
  }
  get(data) {
    // console.log(data);
    this.globalAttributeDataValue = data;
    this.isSelected = true;
    const filteredflowdata = this.getallattribute.filter(flowdata => {
      // if (flowdata.flowId === element.flowId) {
      if (flowdata.attributeName === data) {
        return flowdata;
      }
    });
    // console.log('filteredflowdata', filteredflowdata);
    // console.log('filteredflowdata', filteredflowdata[0].attributeValues);
    this.selectedAttributeValues = filteredflowdata[0].attributeValues;
    // console.log(filteredflowdata.attributeValues);
  }

  addNewValue() {
    const tempData = this.globalAttributeDataValue;
    // console.log(tempData);
    if (this.selectedAttribute === tempData) {
      this.selectedAttributeValues.push(this.newInputValue);
      const filteredflowdata = this.getallattribute.filter(flowdata => {
        // if (flowdata.flowId === element.flowId) {
        if (flowdata.attributeName === tempData) {
          return flowdata;
        }
      });
      // console.log(this.newInputValue);
      // console.log(this.selectedAttributeValues); // It contains new Values
      // console.log('New Filtered Flow Data: ', filteredflowdata);

      // This code converts filteredflowdata Array Object to just an Object.
      const array = filteredflowdata;

      const object_needed = array.find(d => d.flowId === filteredflowdata[0].flowId);
      // console.log(object_needed);


      this.adminService.addNewValueAPI(object_needed).subscribe((data) => {
        this._snackBar.open('Value Added Successfully.', 'Close', {
          duration: 3000,
        });
      }, (error) => {
        // console.log(error.error.status);
        if (error.error.message === 'attribute already present') {
          this._snackBar.open('Value already exist.', 'Close', {
            duration: 3000,
          });
        } else {
          this._snackBar.open('Failed to add Value.', 'Close', {
            duration: 3000,
          });
        }
      });




    }
  }
  deleteValue() {
    const tempData = this.globalAttributeDataValue;
    // console.log(tempData);
    if (this.selectedAttribute === tempData) {
      this.selectedAttributeValues.pop(this.newInputValue);
      const filteredflowdata = this.getallattribute.filter(flowdata => {
        // if (flowdata.flowId === element.flowId) {
        if (flowdata.attributeName === tempData) {
          return flowdata;
        }
      });
      // console.log(this.newInputValue);
      // console.log(this.selectedAttributeValues); // It contains new Values
      // console.log('New Filtered Flow Data: ', filteredflowdata);

      // This code converts filteredflowdata Array Object to just an Object.
      const array = filteredflowdata;

      const object_needed = array.find(d => d.flowId === filteredflowdata[0].flowId);
      // console.log(object_needed);


      this.adminService.addNewValueAPI(object_needed).subscribe((data) => {
        this._snackBar.open('Value Added Successfully.', 'Close', {
          duration: 3000,
        });
      }, (error) => {
        // console.log(error.error.status);
        if (error.error.message === 'attribute already present') {
          this._snackBar.open('Value already exist.', 'Close', {
            duration: 3000,
          });
        } else {
          this._snackBar.open('Failed to add Value.', 'Close', {
            duration: 3000,
          });
        }
      });
    }
  }

}

