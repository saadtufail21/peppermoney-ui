import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Params } from '@angular/router';

import { LoginComponent } from './layout/login/login.component';
import { NewJobComponent } from './layout/new-job/new-job.component';
import { EditJobComponent } from './layout/edit-job/edit-job.component';
import { HistoryComponent } from './layout/history/history.component';
import { TemplateCreationComponent } from './layout/template-creation/template-creation.component';
import { AdminComponent } from './layout/admin/admin.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'new-job', component: NewJobComponent },
  { path: 'edit-job', component: EditJobComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'template-creation', component: TemplateCreationComponent },
  { path: 'admin', component: AdminComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
