import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NewJobComponent } from '../layout/new-job/new-job.component';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  myMethod$: Observable<any>;
  private myMethodSubject = new Subject<any>();

  constructor(private httpClient: HttpClient) {
    this.myMethod$ = this.myMethodSubject.asObservable();

  }
  myMethod(data) {
    let countryvalueData = data;
    // console.log(countryvalueData); // I have data! Let's return it so subscribers can use it!
    // we can do stuff with data if we want
    // this.myMethodSubject.next(data);
  }


  // localUrl = 'http://192.168.1.19:8086/ingestion-service';
  clusterUrl = '/ingestion-service';

  // For localhost testing
  // getTemplateByTemplateList() {
  //   const header = new HttpHeaders().set(
  //     'Authorization',
  //     'Bearer ' + sessionStorage.getItem('token')
  //   );
  //   return this.httpClient.get(
  //     this.localUrl + '/getListOftemplates',
  //     { headers: header }
  //   );
  // }
  // getTemplateByTemplateName(templateName) {
  //   const header = new HttpHeaders().set(
  //     'Authorization',
  //     'Bearer ' + sessionStorage.getItem('token')
  //   );
  //   return this.httpClient.get(
  //     this.localUrl + '/getTemplate/' +
  //     templateName,
  //     { headers: header }
  //   );
  // }

  // uploadConfig(templateName, JsonBody) {
  //   const header = new HttpHeaders().set(
  //     'Authorization',
  //     'Bearer ' + sessionStorage.getItem('token'),
  //   ).set(
  //     'country',
  //    // sessionStorage.getItem('countryCode')
  //      'au'
  //   );
  //   return this.httpClient.post(
  //     this.localUrl + '/pattern/' + templateName + '/flow', JsonBody,
  //     { headers: header }
  //   );
  // }




  // For cluster Testing
  getTemplateByTemplateList() {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(
      this.clusterUrl + '/getListOftemplates',
      { headers: header }
    );
  }
  getTemplateByTemplateName(templateName) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(
      this.clusterUrl + '/getTemplate/' +
      templateName,
      { headers: header }
    );
  }

  uploadConfig(templateName, JsonBody) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token'),
    ).set(
      'country',
      // sessionStorage.getItem('countryCode')
      'au'
    );

    return this.httpClient.post(
      this.clusterUrl + '/pattern/' + templateName + '/flow', JsonBody,
      { headers: header }
    );
  }





}

