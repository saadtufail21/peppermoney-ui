import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NewJobComponent } from '../layout/new-job/new-job.component';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  countryCode: string;
  // historyAPI = 'http://192.168.1.19:8086/ingestion-service/'; // Fetch All Job History
  historyAPI = '/ingestion-service/'; // For Cluster Deployement

  constructor(private httpClient: HttpClient) {
  }

  getAllJobs() {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(this.historyAPI + 'getAllflowDetails', { headers: header });
  }

  adhocJob(processGroupId, params) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    ).set(
      'country',
      // sessionStorage.getItem('countryCode')
      'au'
    );
    // console.log('Job Service - Params', params);
    // console.log('Job Service processGroupId', processGroupId);
    // console.log(sessionStorage.getItem('countryCode'));
    // console.log(this.historyAPI + '/adhoc/' + processGroupId, { headers: header });
    return this.httpClient.post(this.historyAPI + 'adhoc/' + processGroupId, params, { headers: header });
  }

  deleteJob(processGroupId) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    ).set(
      'country',
      // sessionStorage.getItem('countryCode')
      'au'
    );
    // console.log('Job Service processGroupId', processGroupId);
    // console.log(sessionStorage.getItem('countryCode'));
    // console.log(this.historyAPI + '/flow/' + processGroupId, { headers: header });
    return this.httpClient.delete(this.historyAPI + 'flow/' + processGroupId, { headers: header });
  }

  getEditJobDetails(processGroupId) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(this.historyAPI + 'getflowDetails/' + processGroupId, { headers: header });

  }
  editJobUploadConfig(flowId, editJobJson) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    ).set(
      'country',
      // sessionStorage.getItem('countryCode')
      'au'
    );
    return this.httpClient.post(this.historyAPI + 'pattern/editFlow/' + flowId, editJobJson, { headers: header });

  }


  // Start Stopped Scheduled Job
  startRescheduleJob(processGroupId) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.post(this.historyAPI + '/reschedule/' + processGroupId, null, { headers: header });
  }


  // Stop Running Scheduled Job
  stopRescheduleJob(processGroupId) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.post(this.historyAPI + '/killOoziejob/' + processGroupId, null, { headers: header });
  }

}
