import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  adminAPI = '/ingestion-service/'; // For Cluster Deployement

  constructor(private httpClient: HttpClient) { }

  getAllAttributes() {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(this.adminAPI + 'getAllAdminAttributes', { headers: header });
  }

  getAttributesValues(attributeId) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.get(this.adminAPI + '/getAdminAttribute/' + attributeId, { headers: header });
  }

  addNewValueAPI(attributeJson) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.post(this.adminAPI + 'saveAdminAttributes', attributeJson, { headers: header });
  }

  editValueAPI() {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.post(this.adminAPI + 'saveAdminAttributes', { headers: header });
  }

  deleteValueAPI(attributeJson) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    );
    return this.httpClient.post(this.adminAPI + 'saveAdminAttributes', attributeJson, { headers: header });
  }

}
