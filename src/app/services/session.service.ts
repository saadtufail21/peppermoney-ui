import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  // To pass the username to NavBar
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();

  // loginAPI = 'http://192.168.1.19:8086/ingestion-service/'; // fill this with the url value

  loginAPI = '/ingestion-service/'; // // For Cluster Deployement
  constructor(private httpClient: HttpClient) { }

  login(params) {
    // console.log(params);
    this.getLoggedInName.emit(params.username);
    return this.httpClient.post(this.loginAPI + 'auth/token', params);
  }


}
