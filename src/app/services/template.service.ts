import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  // urlAPI = 'http://192.168.1.19:8086/ingestion-service/saveTemplate'; // fill this with the url value
  urlAPI = '/ingestion-service/';  // For Cluster Deployement
  constructor(private httpClient: HttpClient) { }

  addTemplate(param) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    ).append('content-type', 'application/json');
    return this.httpClient.post(this.urlAPI + 'saveTemplate', param, { headers: header });
  }

  addDDLToAdmin(param) {
    const header = new HttpHeaders().set(
      'Authorization',
      'Bearer ' + sessionStorage.getItem('token')
    ).append('content-type', 'application/json');
    return this.httpClient.post(this.urlAPI + 'saveAllAdminAttributes', param, { headers: header });
  }


}
